package processor;

import main.Roxel;

import org.openspaces.events.EventDriven;
import org.openspaces.events.EventTemplate;
import org.openspaces.events.adapter.SpaceDataEvent;
import org.openspaces.events.notify.Notify;

@EventDriven @Notify
public class SimpleListener {

    @EventTemplate
    Roxel unprocessedData() {
    	Roxel template = new Roxel();
        //template.setProcessed(false);
    	template.setDirEast(true);
        return template;
    }

    @SpaceDataEvent
    public void eventListener(Roxel event) {
        //process Data here
    	System.out.println(event);
    }
}
