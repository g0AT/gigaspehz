package processor;

import java.util.HashMap;
import java.util.Map;

import main.Roxel;
import main.RoxelInfo;
import main.SimulationParameters;
import main.SimulationState;
import main.trafficlight.DirectionLease;
import main.trafficlight.MediumIntelligentTrafficLightSwitschingAlgorithm;
import main.trafficlight.TrafficLightSignal;
import main.trafficlight.TrafficLightSwitchingAlgorithm;

import org.openspaces.core.GigaSpace;
import org.openspaces.core.context.GigaSpaceContext;
import org.openspaces.events.EventDriven;
import org.openspaces.events.EventTemplate;
import org.openspaces.events.adapter.SpaceDataEvent;
import org.openspaces.events.notify.Notify;
import org.openspaces.events.notify.NotifyType;
import org.springframework.beans.factory.InitializingBean;

import com.gigaspaces.client.WriteModifiers;
import com.j_spaces.core.client.SQLQuery;

/**
 * The processor is passed interesting Objects from its associated PollingContainer
 * <p>
 * The PollingContainer removes objects from the GigaSpace that match the criteria specified for it.
 * <p>
 * Once the Processor receives each Object, it modifies its state and returns it to the PollingContainer which writes them back to the GigaSpace
 * <p/>
 * <p>
 * The PollingContainer is configured in the pu.xml file of this project
 * 
 */
 @EventDriven
// @Polling
 @Notify(performTakeOnNotify = true)
 @NotifyType(write = true, update = true)
public class TrafficLight implements InitializingBean{

	@GigaSpaceContext(name = "gigaSpace")
    private GigaSpace                      space;
	private Boolean jedone = false;
    private TrafficLightSwitchingAlgorithm switchingAlgo;
    private Map<Long, RoxelInfo>           roxelIdRoxelInfoMap = new HashMap<>();

    
    private void initIt(){
    	System.out.println("initIt");
        switchingAlgo = new MediumIntelligentTrafficLightSwitschingAlgorithm(space);
        System.out.println("Processor instantiated, waiting for messages feed...");
        for (RoxelInfo ri : space.readMultiple(new RoxelInfo())) {
            roxelIdRoxelInfoMap.put(ri.getRoxelId(), ri);
        }
        
        System.out.println("initialized TrafficLight " + roxelIdRoxelInfoMap.size() );
        System.out.println("initIt-ENDE");
    }
    public void afterPropertiesSet() throws Exception {
    	
    	System.out.println("afterPropertiesSet()");
    	
    	SimulationState stateToWaitFor = new SimulationState();
    	stateToWaitFor.setState(SimulationState.State.MAP_CREATED);
    	SimulationState currentState;
    	do{
    		currentState = space.take(stateToWaitFor);
    	}while(currentState == null);

    	initIt();
    	
    	currentState.setPuCnt(currentState.getPuCnt() - 1);
    	if(currentState.getPuCnt() <= 0){
    		SimulationState simState = new SimulationState();
    		simState.setState(SimulationState.State.PU_INITIALIZED);
    		space.write(simState);
    	}else{
    		space.write(currentState);
    	}
    }
    
//    public TrafficLight(GigaSpace space) {
//        this.space = space;
//        switchingAlgo = new MediumIntelligentTrafficLightSwitschingAlgorithm(space);
//        System.out.println("Processor instantiated, waiting for messages feed...");
//        for (RoxelInfo ri : space.readMultiple(new RoxelInfo())) {
//            roxelIdRoxelInfoMap.put(ri.getRoxelId(), ri);
//        }
//    }
    
    public TrafficLight() {
    	System.out.println("TrafficLight()");
    }

     @EventTemplate
     public SQLQuery<Roxel> untouchedRoxel(){
    	 return new SQLQuery<Roxel> (Roxel.class , "dirNorth = false AND dirEast = false AND dirSouth = false AND dirWest = false");
     }

    /**
     * Process the given Message and return it to the caller. This method is invoked using OpenSpaces Events when a matching event occurs.
     */
    @SpaceDataEvent
    public Roxel processMessage(Roxel rx) {
//    	synchronized (jedone) {
//        	if(!jedone){
//        		initIt();
//        		jedone = true;
//        	}
//		}

    	RoxelInfo ri = roxelIdRoxelInfoMap.get(rx.getId());
        String query = "position.x=" + ri.getPosition().getX() + " AND position.y=" + ri.getPosition().getY(); 
        //TrafficLightSignal tls = space.readIfExists(new SQLQuery<TrafficLightSignal>(TrafficLightSignal.class, query));
        TrafficLightSignal tls = space.take(new SQLQuery<TrafficLightSignal>(TrafficLightSignal.class, query));
        if(tls == null){
            tls = new TrafficLightSignal();
            System.out.println("--->there is no traffic light signal!!");
        }
    	
    	DirectionLease dirLease = tls.getDirLease();
        
    	if(dirLease == null || (dirLease.getLease()-1) <= 0){
    		try {
				Thread.sleep(SimulationParameters.TRAFFIC_LIGHT_SWITCHING_DELAY_MS);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		dirLease = switchingAlgo.getNextDirection(rx);
    		
        }else{
        	dirLease.setLease(dirLease.getLease() - 1);
        }
    	
    	rx.setDirections(dirLease.getDirs());
    	
    	dirLease.setLease(dirLease.getLease());
        tls.setPosition(ri.getPosition());
        tls.setDirection(dirLease.getDirs());
        tls.setDirLease(dirLease);
        space.write(tls, WriteModifiers.UPDATE_OR_WRITE);
        return rx;
    }

}
