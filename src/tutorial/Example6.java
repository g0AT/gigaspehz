package tutorial;

import jgame.*;
import jgame.platform.*;

/** Tutorial example 6: scrolling and wrapping. */
public class Example6 extends JGEngine {

	private static final int ROXEL_SIZE = 32;
	private static final int WORLD_SIZE_X = 40;
	private static final int WORLD_SIZE_Y = 40;

	private static final int DISTANCE_BETW_STREETS_X = 5;
	private static final int DISTANCE_BETW_STREETS_Y = 5;

	public static void main(String[] args) {
		new Example6(new JGPoint(768, 768));
	}

	/** Application constructor. */
	public Example6(JGPoint size) {
		initEngine(size.x, size.y);
	}

	/** Applet constructor. */
	public Example6() {
		initEngineApplet();
	}

	public void initCanvas() {
		setCanvasSettings(WORLD_SIZE_X, WORLD_SIZE_Y, 32, 32, null, null, null);
	}

	public void initGame() {
		// we increase the framerate a bit because it is more critical
		// for scrolling.
		setFrameRate(45, 2);
		// load a single sprite
		defineImage("car", // graphic name
				"c", 1, // tile name and tile cid (in case we use it as a tile)
				"./res/pic/auto32.png", // file
				"-" // graphical operation (may be one or two out of
					// "x","y", "l","r","u")
		);

		defineImage("street_h", // graphic name
				"=", 2, // tile name and tile cid (in case we use it as a tile)
				"./res/pic/street32_h.png", // file
				"-" // graphical operation (may be one or two out of
					// "x","y", "l","r","u")
		);

		defineImage("street_v", // graphic name
				"|", 2, // tile name and tile cid (in case we use it as a tile)
				"./res/pic/street32_v.png", // file
				"-" // graphical operation (may be one or two out of
					// "x","y", "l","r","u")
		);

		defineImage("street_hv", // graphic name
				"#", 2, // tile name and tile cid (in case we use it as a tile)
				"./res/pic/street32_hv.png", // file
				"-" // graphical operation (may be one or two out of
					// "x","y", "l","r","u")
		);
		
//		defineImage("street_hvTile", // graphic name
//				"hv", 4, // tile name and tile cid (in case we use it as a tile)
//				"./res/pic/street32_hv.png", // file
//				"-" // graphical operation (may be one or two out of
//					// "x","y", "l","r","u")
//		);
		
		// load the background image
		defineImage("empty", ".", 0, "./res/pic/empty32.gif", "-");
		setBGImage("empty");
		
		
		// hide the mouse cursor
		// setCursor(null);
		// Increase the playfield size to obtain a scrollable, wrappable area.
		// It is twice the width of the view, and two tiles higher. The two
		// tiles are used as space for objects to disappear into before
		// wrapping around. Without this space, objects would visibly jump
		// from top to bottom or vice versa.
		setPFSize( WORLD_SIZE_X , WORLD_SIZE_Y);
		// Set the wrap-around mode to vertical.
		setPFWrap(false, // horizontal wrap
				false, // vertical wrap
				-10, -10 // shift the center of the view to make objects wrap at
							// the right moment (sprite size / 2).
		);
		
		
		String[] tileMap = new String[WORLD_SIZE_Y];
		
		for (int x = 0; x < WORLD_SIZE_X; x++) {
			
			tileMap[x] = new String();

			for (int y = 0; y < WORLD_SIZE_Y; y++) {
				
				if ( ( (x % DISTANCE_BETW_STREETS_X) == 0  )
					 && 
					 ( (y % DISTANCE_BETW_STREETS_Y) == 0  ) ) {
					
					tileMap[x] += "#";
					setTile(x, y, "#");

				} else if ( (y % DISTANCE_BETW_STREETS_Y) == 0) {
					tileMap[x] += "|";
					setTile(x, y, "=");

				}else if( (x % DISTANCE_BETW_STREETS_X) == 0 ){
					tileMap[x] += "=";
					setTile(x, y, "|");
				}else{
					tileMap[x] += ".";
					setTile(x, y, ".");
				}
				

			}
			
			
			
		}
		
		//setTilesMulti(0, 0, tileMap);
		//setTiles(0, 0, tileMap);
		
		
		

//		for (int x = 0; x < WORLD_SIZE_X * ROXEL_SIZE; x++) {
//
//			for (int y = 0; y < WORLD_SIZE_Y * ROXEL_SIZE; y++) {
//
//				if ((x % (DISTANCE_BETW_STREETS_X * ROXEL_SIZE) == 0)
//						&& (y % (DISTANCE_BETW_STREETS_Y * ROXEL_SIZE) == 0)) {
//					
//					//setTile(x, y, "#");
//					
//					//setTile(x+1, y+1, "=");
//					//setTilesMulti(x, y, new String[]{"#" , "=", "|" });
//					
////					new JGObject("crossing", true, // name
////							x, y, // pos
////							2, "street_hv", // cid, sprite
////							0.0, 0.0, // speed
////							JGObject.expire_never // Suspend when off view.
////					// Suspended objects will not move or participate in
////					// collisions.
////					);
//				} else if ( (x % ROXEL_SIZE == 0) && (y % (DISTANCE_BETW_STREETS_Y * ROXEL_SIZE) == 0)) {
////					new JGObject("strh", true, // name
////							x, y, // pos
////							3, "street_h", // cid, sprite
////							0.0, 0.0, // speed
////							JGObject.expire_never // Suspend when off view.
////					// Suspended objects will not move or participate in
////					// collisions.
////					);
//				}else if((y % ROXEL_SIZE == 0) && (x % (DISTANCE_BETW_STREETS_X * ROXEL_SIZE) == 0)){
////					new JGObject("strv", true, // name
////							x, y, // pos
////							4, "street_v", // cid, sprite
////							0.0, 0.0, // speed
////							JGObject.expire_never // Suspend when off view.
////					// Suspended objects will not move or participate in
////					// collisions.
////					);
//				}
//
//			}
//
//		}

		// place some random objects that move up and down
		for (int i = 0; i < 40; i++) {
			
			int x = ((int)random(0,(pfWidth()  / (ROXEL_SIZE * DISTANCE_BETW_STREETS_X) ))) * ROXEL_SIZE * DISTANCE_BETW_STREETS_X;
			int y = ((int)random(0,(pfHeight() / (ROXEL_SIZE * DISTANCE_BETW_STREETS_Y) ))) * ROXEL_SIZE * DISTANCE_BETW_STREETS_Y;
			
			double speedX;
			double speedY;
			
			if(random(0.0 , 1.0) > 0.5){
				//speedX = random(0.0 , 1.0);
				speedX = 5.0;
				speedY = 0.0;
			}else{
				speedX = 0.0;
				//speedY = random(0.0 , 1.0);
				speedY = 5.0;
			}
			
			new JGObject("muyobj", true, // name
					x, y, // pos
					1, "car", // cid, sprite
					speedX, speedY, // speed
					JGObject.expire_never // Suspend when off view.
			// Suspended objects will not move or participate in
			// collisions.
			);
		}

	}

	/** View offset. */
	int xofs = 0, yofs = 0;

	public void doFrame() {
		moveObjects(null, 0);
		// Update the desired view offset according to mouse position.
		// The X offset is proportional to the mouse position.
		// The mouse position is a number between 0 and viewWidth.
		// We scale it to a number between 0 and pfWidth (playfield width).
		xofs = getMouseX() * pfWidth() / viewWidth();
		// the Y offset changes proportional to the offset of the mouse
		// position from the center of the window. If the mouse is in the
		// center, we don't scroll, if it is close to the upper or lower
		// border of the window, it scrolls quickly in that direction.
		// yofs += (getMouseY() - viewHeight()/2) / 15;
		yofs = getMouseY() * pfHeight() / viewHeight();
		// Set the view offset. Note that if our offset is out of the
		// playfield bounds, the position is clipped so that it is inside.
		// (this is only relevant for non-wrappable axes; a wrappable
		// axis is never out of bounds!)
		setViewOffset(xofs, yofs, // the position within the playfield
				true // true means the given position is center of the view,
						// false means it is topleft.
		);
	}

	public void paintFrame() {
		setColor(JGColor.black);
		setFont(new JGFont("Arial", 0, 20));
		// draw some info (note: the text is drawn relative to the view,
		// rather than the playfield)
		drawString("Move the mouse to scroll.", viewWidth() / 2, 80, 0);
		drawString("Playfield offset is now (" + xofs + "," + yofs + ").",
				viewWidth() / 2, 120, 0);
		// Indicate the corners of the playfield. This text should be drawn
		// relative to the playfield. The other draw methods can also draw
		// both relative to playfield and to view. For text, relative to view
		// is the default; for other draw methods, relative to playfield is
		// default.
		drawString("TOP LEFT", 0, 8, -1, true // indicate it should be drawn
												// relative to playfield
		);
		drawString("BOTTOM LEFT", 0, pfHeight() - 20, -1, true);
		drawString("TOP RIGHT", pfWidth(), 8, 1, true);
		drawString("BOTTOM RIGHT", pfWidth(), pfHeight() - 20, 1, true);

	}
}
