package main;

import gui.SimulationView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.openspaces.core.GigaSpace;
import org.openspaces.core.GigaSpaceConfigurer;
import org.openspaces.core.space.UrlSpaceConfigurer;

public class Testframe {
	
	public static void main(String[] args) throws InterruptedException {
		
		//GigaSpace tupelSpace = new GigaSpaceConfigurer(new UrlSpaceConfigurer(SimulationParameters.TUPEL_SPACE_URI)).defaultTakeTimeout(10000).gigaSpace();
//		String gsRemoteURL = "jini://*/*/TrafficCoordinationSpace";  //remote space
	    GigaSpace tupelSpace = new GigaSpaceConfigurer(new UrlSpaceConfigurer(SimulationParameters.TUPEL_SPACE_URI_REMOTE)).defaultTakeTimeout(10000).gigaSpace();

		
		//create the map and load it to the tupelSpace
		for (int x = 0; x < SimulationParameters.WORLD_ROXEL_CNT_X; x++) {
			
			for (int y = 0; y < SimulationParameters.WORLD_ROXEL_CNT_Y; y++) {
				
				RoxelInfo ri = null;
				Roxel     r  = null;
				
				if ( ( (x % (SimulationParameters.DISTANCE_BETW_STREETS_X + 1)) == 0  )
					 && 
					 ( (y % (SimulationParameters.DISTANCE_BETW_STREETS_Y + 1)) == 0  ) ) {
					r = new Roxel(Direction.north, Direction.east, Direction.south, Direction.west);
					ri = new RoxelInfo(r, new Position(x, y), Direction.north, Direction.east, Direction.south, Direction.west);

				} else if ( (y % (SimulationParameters.DISTANCE_BETW_STREETS_Y + 1)) == 0) {
					r = new Roxel(Direction.east);
					ri = new RoxelInfo(r, new Position(x, y), Direction.east);

				}else if( (x % (SimulationParameters.DISTANCE_BETW_STREETS_X + 1)) == 0 ){
					r = new Roxel(Direction.south);
					ri = new RoxelInfo(r, new Position(x, y), Direction.south);
				}else{
					r = new Roxel(Direction.blocked);
					ri = new RoxelInfo(r, new Position(x, y), Direction.blocked);
				}
				
				r.setRoxelInfoId(r.getRoxelInfoId());
				tupelSpace.write(ri);
				tupelSpace.write(r);
			}
		}
		
		SimulationState simStateMapInitialized = new SimulationState();
		simStateMapInitialized.setPuCnt(1L);
		simStateMapInitialized.setState(SimulationState.State.MAP_CREATED);
		tupelSpace.write(simStateMapInitialized);		
		
//		SQLQuery<Roxel> query = new SQLQuery<Roxel> (Roxel.class , "dirNorth = false AND dirEast = false AND dirSouth = false AND dirWest = false");
//		
//		SimpleNotifyEventListenerContainer notifyEventListenerContainer =
//			new SimpleNotifyContainerConfigurer(tupelSpace).template(query).eventListenerAnnotation(new TrafficLight(tupelSpace))
//			.notifyWrite(true).notifyUpdate(true).notifyContainer();
//		notifyEventListenerContainer.start();

    	SimulationState stateToWaitFor = new SimulationState();
    	stateToWaitFor.setState(SimulationState.State.PU_INITIALIZED);
    	SimulationState currentState;
    	do{
    		currentState = tupelSpace.take(stateToWaitFor);
    	}while(currentState == null);
    	
    	System.out.println("State: PU_INITIALIZED");
		
		new SimulationView(SimulationParameters.WORLD_ROXEL_CNT_X, SimulationParameters.WORLD_ROXEL_CNT_Y, tupelSpace);
		
		Thread.sleep(3000);
		
		Random random = new Random();
		List<Client> carCLients = new ArrayList<Client>();
		
		// create some Cients using the space
		for (int i = 0; i < SimulationParameters.CAR_COUNT; i++) {
			
			//start on a valid street coordinate
			int x = random.nextInt(SimulationParameters.WORLD_ROXEL_CNT_X  / (SimulationParameters.DISTANCE_BETW_STREETS_X + 1) ) * (SimulationParameters.DISTANCE_BETW_STREETS_X + 1);
			int y = random.nextInt(SimulationParameters.WORLD_ROXEL_CNT_Y  / (SimulationParameters.DISTANCE_BETW_STREETS_Y + 1) ) * (SimulationParameters.DISTANCE_BETW_STREETS_X + 1);

			Direction dir;
			
			if(random.nextBoolean()){ // cars drive either EAST or SOUTH
				dir = Direction.east;
			}else{
				dir = Direction.south;
			}
			
			carCLients.add(new Client(tupelSpace, x, y, dir, SimulationParameters.CAR_SPEED_METER_PER_SECOND * (SimulationParameters.CAR_SPEED_METER_PER_SECOND_VARIATION * Math.random() + 1 )));
		}
		
		for(Client cl: carCLients){
			Thread t = new Thread(cl);
			t.start();
			
			try {
				Thread.sleep(random.nextInt(30));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		int x = 0;
		while(x == 0){
			Thread.sleep(5/*s*/ * 1000/*ms*/);
		} //wait forever
		
//		for(Client cl: carCLients){
//			cl.requestTermination();
//		}

	}

}
