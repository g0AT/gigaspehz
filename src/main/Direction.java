package main;

import java.io.Serializable;

public class Direction implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final Direction north = new Direction(0b00000001);
	public static final Direction east = new Direction(0b00000010);
	public static final Direction south = new Direction(0b00000100);
	public static final Direction west = new Direction(0b00001000);
	public static final Direction blocked = new Direction(0b00010000);
	public static final Direction toDecide = new Direction(0b00100000);

	private int properties;

	public Direction() {

	}

	private Direction(int properties) {
		this.properties = properties;
	}

	public int getProperties() {
		return properties;
	}

	public void setProperties(int properties) {
		this.properties = properties;
	}

	public static Direction create(Direction... directions) {

		int combined = 0;
		for (Direction d : directions) {
			combined |= d.properties;
		}

		return new Direction(combined);
	}

	@Override
	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = prime * result + properties;
//		return result;
		return 2;
	}

	@Override
	public boolean equals(Object other) {
		if (other != null && other instanceof Direction) {
			Direction oth = (Direction) other;
			if (oth.properties != 0 && (this.properties & oth.properties) > 0) {
				return true;
			}
		}
		return false;
	}

	public String toString() {
		String s = "Direction[ ";
		if ((properties & north.properties) > 0)
			s += "north ";
		if ((properties & east.properties) > 0)
			s += "east ";
		if ((properties & south.properties) > 0)
			s += "south ";
		if ((properties & west.properties) > 0)
			s += "west ";
		if ((properties & blocked.properties) > 0)
			s += "blocked ";
		if ((properties & toDecide.properties) > 0)
			s += "toDecide ";
		return s + "] ";
	}
}
