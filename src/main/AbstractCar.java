package main;

import com.gigaspaces.annotation.pojo.SpaceClass;
import com.gigaspaces.annotation.pojo.SpaceId;

@SpaceClass
public abstract class AbstractCar {
		
	private static Long IDCOUNTER = new Long(0);
	public static final EmptyCar EMPTY_CAR = new EmptyCar();
	
	private Long id;
	
	private Double speedMeterPerSecond;
	private Position position;
	private Direction direction;
	
	public AbstractCar() {
	}
	
    public AbstractCar(Position position, Double speedMs, Direction direction){
		this.id = IDCOUNTER++;
		this.position = position;
		this.speedMeterPerSecond = speedMs;
		this.direction = direction;
	}

	public Double getSpeedMeterPerSecond() {
		return speedMeterPerSecond;
	}

	public void setSpeedMeterPerSecond(Double speedMeterPerSecond) {
		this.speedMeterPerSecond = speedMeterPerSecond;
	}
	
	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}
	
	@SpaceId(autoGenerate = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractCar other = (AbstractCar) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AbstractCar [id=" + id + ", speedMeterPerSecond="
				+ speedMeterPerSecond + ", position=" + position
				+ ", direction=" + direction + "]";
	}

	
	
	
	
}
