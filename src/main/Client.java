package main;

import java.util.HashMap;
import java.util.Map;

import org.openspaces.core.GigaSpace;

import com.gigaspaces.client.WriteModifiers;

public class Client implements Runnable {

	private static boolean PRINT_DEBUG = false;

	// private final int myID;

	private GigaSpace tupelSpace;

	// private Position currentPosition;

	private double speedMs;

	private boolean wantToMoveForward;
	private Roxel currentRoxel;

	private Map<Position, RoxelInfo> worldMap = new HashMap<>();

	Car myCar;

	public Client(GigaSpace tupelSpace, int initialXpos, int initialYpos, Direction direction, double initialSpeedMs) {
		this.tupelSpace = tupelSpace;
		// this.currentPosition = new Position(initialXpos, initialYpos);

		this.speedMs = initialSpeedMs;

		this.wantToMoveForward = true;

		myCar = new Car(new Position(initialXpos, initialYpos), speedMs,
				direction);

		for (RoxelInfo ri : tupelSpace.readMultiple(new RoxelInfo())) {
			worldMap.put(ri.getPosition(), ri);
		}
	}

	private void moveThroughRoxel(RoxelInfo roxelInfo) {
		try {
			printDebug("Car [" + myCar.getId() + "]: driving through roxel " + roxelInfo.getPosition());
			Thread.sleep((long) (roxelInfo.getLengthM() / myCar.getSpeedMeterPerSecond() * 1000.0));
			printDebug("Car [" + myCar.getId() + "]: reached end of roxel " + roxelInfo.getPosition());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void run() {
		Roxel lastRoxel = null;
		Roxel wantedRoxel = new Roxel();

		while (wantToMoveForward) {

			Position nextPosition = nextPosition(myCar.getPosition());
			wantedRoxel.setId(worldMap.get(nextPosition).getRoxelId());
			wantedRoxel.setCarId(Car.EMPTY_CAR.getId());
			// wantedRoxel.setDirection(myCar.getDirection());
			wantedRoxel.setDirections(myCar.getDirection());

			do {
				printDebug("want Roxel: " + wantedRoxel.toString());
				currentRoxel = tupelSpace.take(wantedRoxel);
				printDebug("got Roxel: " + currentRoxel);
			} while (currentRoxel == null);

			// write back as empty ---vv
			if (lastRoxel != null) {
				lastRoxel.setCarId(Car.EMPTY_CAR.getId());
				if (worldMap.get(myCar.getPosition()).isCrossing()) {
					
					lastRoxel.setDiretionToDecide();
//					System.out.println("writing back roxel: " + lastRoxel);
				}
				tupelSpace.write(lastRoxel);
				// System.out.println("Car[" + myCar.getId() + "] " + lastRoxel
				// +" written back as Empty");
				printDebug("wrote back empty Roxel @ " + lastRoxel.toString());
			}
			// write back as empty ---^^

			// ######### write back as occupied (allows monitoring) vvvvv
			currentRoxel.setCarId(myCar.getId());
			tupelSpace.write(currentRoxel);
			// myCar.setPosition(newRoxelInfo.getPosition());
			myCar.setPosition(nextPosition);
			tupelSpace.write(myCar, WriteModifiers.UPDATE_OR_WRITE);
			// System.out.println("Car[" + myCar.getId() + "] " + currentRoxel +
			// " written back occupied");
			// ######### write back as occupied (allows monitoring) vvvvv

			moveThroughRoxel(worldMap.get(myCar.getPosition()));
			printDebug("moved through: " + wantedRoxel.toString());

			lastRoxel = currentRoxel;

		}
	}

	// public Roxel initialJumpToTraffic(){
	//
	// }

	public void requestTermination() {
		wantToMoveForward = false;
	}

	private Position nextPosition(Position currentPosition) {

		int currentXpos = currentPosition.getX();
		int currentYpos = currentPosition.getY();

		int newX = currentXpos, newY = currentYpos;

		if (myCar.getDirection().equals(Direction.north)) {
			newY = (currentYpos == 0) ? SimulationParameters.WORLD_ROXEL_CNT_Y
					: currentYpos - 1;
		} else if (myCar.getDirection().equals(Direction.south)) {
			newY = (currentYpos + 1) % SimulationParameters.WORLD_ROXEL_CNT_Y;
		} else if (myCar.getDirection().equals(Direction.east)) {
			newX = (currentXpos + 1) % SimulationParameters.WORLD_ROXEL_CNT_X;
		} else if (myCar.getDirection().equals(Direction.west)) {
			newX = (currentXpos == 0) ? SimulationParameters.DISTANCE_BETW_STREETS_X
					: currentXpos - 1;
		}

		return new Position(newX, newY);
	}

	// @Transactional
	// public void moveToNextRoxel(Roxel nextRoxel) {
	//
	// Roxel oldRoxel = currentRoxel;
	//
	// // ######### take next roxel if free (template matching) vvvvv
	// printDebug("Car [" + myID + "]: want to switch " + currentRoxel.getPos()
	// + " -> " + nextRoxel.getPos());
	//
	// do{
	// currentRoxel = tupelSpace.take(nextRoxel); // take the next roxel from
	// tupelspace
	// }while (currentRoxel == null); // the wanted roxel couldn't be received..
	//
	// printDebug("Car [" + myID + "]: got Roxel: " + currentRoxel);
	// // ######### take next roxel if free (template matching) ^^^^^
	// //myCar.setSpeedMeterPerSecond(new Random().nextDouble() * 1000.0 + 10);
	//
	// // ######### write back as occupied (allows monitoring) vvvvv
	// myCar.setPosition(currentRoxel.getPos());
	// currentRoxel.setCar(myCar);
	// tupelSpace.write(currentRoxel);
	//
	// //update the car object with the new position
	// tupelSpace.write(myCar, WriteModifiers.UPDATE_OR_WRITE);
	// printDebug("Car [" + myID + "]: wrote back new roxel as occupied: " +
	// oldRoxel);
	// // ######### write back as occupied (allows monitoring) ^^^^^
	//
	// // write previous roxel back indicated empty vvvvvv
	// oldRoxel.setCar(new EmptyCar());
	// tupelSpace.write(oldRoxel);
	// printDebug("Car [" + myID + "]: wrote back empty roxel: " + oldRoxel);
	// // write previous roxel back indicated empty ^^^^^^
	// }

	public void printDebug(String msg) {
		if (PRINT_DEBUG) {
			System.out.println(msg);
		}
	}

}
