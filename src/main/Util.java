package main;

import main.idgen.IdTupel;

import org.openspaces.core.GigaSpace;

public class Util {
	private GigaSpace space;

	public Util(GigaSpace space) {
		this.space = space;
		IdTupel idt = space.read(new IdTupel());
		if (idt == null) {
			space.write(new IdTupel(0L));
		}
	}

	public Long generateId() {
		IdTupel idt = null;
		do {
			idt = space.take(new IdTupel());
		} while (idt == null);
		Long ret = idt.getId();
		idt.setId(++ret);
		space.write(idt);
		return ret;
	}

}
