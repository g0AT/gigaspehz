package main;

import com.gigaspaces.annotation.pojo.SpaceClass;
import com.gigaspaces.annotation.pojo.SpaceId;

@SpaceClass
public class Roxel {
	
	
	private Integer routingId;
	
	private static Long IDCOUNTER = new Long(100000);
	
	private Long id = null;
	
	private Long carId;
//	private Long roxelInfoId;
//	private Direction direction;
	
	private Boolean dirNorth;
	private Boolean dirEast;
	private Boolean dirSouth;
	private Boolean dirWest;
	
	private Long roxelInfoId;
	
	
	public Long getRoxelInfoId() {
		return roxelInfoId;
	}

	public void setRoxelInfoId(Long roxelInfoId) {
		this.roxelInfoId = roxelInfoId;
	}

	public Roxel(){
		
	}
	
	
	public Roxel(Direction ... directions){
		this.id = IDCOUNTER++;
		carId = Car.EMPTY_CAR.getId();
		setDirections(directions);
	}

//	@SpaceRouting
	public Integer getRoutingId() {
		return routingId;
	}

	public void setRoutingId(Integer routingId) {
		this.routingId = routingId;
	}

	@SpaceId( autoGenerate = false )
	public Long getId() {
		return id;
	}

	public Boolean getDirNorth() {
		return dirNorth;
	}

	public void setDirNorth(Boolean dirNorth) {
		this.dirNorth = dirNorth;
	}

	public Boolean getDirEast() {
		return dirEast;
	}

	public void setDirEast(Boolean dirEast) {
		this.dirEast = dirEast;
	}

	public Boolean getDirSouth() {
		return dirSouth;
	}

	public void setDirSouth(Boolean dirSouth) {
		this.dirSouth = dirSouth;
	}

	public Boolean getDirWest() {
		return dirWest;
	}

	public void setDirWest(Boolean dirWest) {
		this.dirWest = dirWest;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCarId() {
		return carId;
	}

	public void setCarId(Long carId) {
		this.carId = carId;
	}
	
	public void setDirections(Direction ... directions){
		dirNorth = null;
		dirEast  = null;
		dirSouth = null;
		dirWest  = null;
		
		for(Direction d: directions){
			if(d.equals(Direction.north)){
				dirNorth = true;
			}else if(d.equals(Direction.east)){
				dirEast = true;
			}else if(d.equals(Direction.south)){
				dirSouth = true;
			}else if(d.equals(Direction.west)){
				dirWest = true;
			}else if(d.equals(Direction.toDecide)){
				dirNorth = false;
				dirEast  = false;
				dirSouth = false;
				dirWest  = false;
				return;
			}
		}
	}
	
	public void setDiretionToDecide(){
		dirNorth = false;
		dirEast  = false;
		dirSouth = false;
		dirWest  = false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dirEast == null) ? 0 : dirEast.hashCode());
		result = prime * result
				+ ((dirNorth == null) ? 0 : dirNorth.hashCode());
		result = prime * result
				+ ((dirSouth == null) ? 0 : dirSouth.hashCode());
		result = prime * result + ((dirWest == null) ? 0 : dirWest.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Roxel other = (Roxel) obj;
		if (dirEast == null) {
			if (other.dirEast != null)
				return false;
		} else if (!dirEast.equals(other.dirEast))
			return false;
		if (dirNorth == null) {
			if (other.dirNorth != null)
				return false;
		} else if (!dirNorth.equals(other.dirNorth))
			return false;
		if (dirSouth == null) {
			if (other.dirSouth != null)
				return false;
		} else if (!dirSouth.equals(other.dirSouth))
			return false;
		if (dirWest == null) {
			if (other.dirWest != null)
				return false;
		} else if (!dirWest.equals(other.dirWest))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Roxel [id=" + id + ", carId=" + carId + ", dirNorth="
				+ dirNorth + ", dirEast=" + dirEast + ", dirSouth=" + dirSouth
				+ ", dirWest=" + dirWest + "]";
	}


	
	

//	public Long getRoxelInfoId() {
//		return roxelInfoId;
//	}
//
//	public void setRoxelInfoId(Long roxelInfoId) {
//		this.roxelInfoId = roxelInfoId;
//	}



	
	
	
	
}
