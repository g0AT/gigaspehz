package main.trafficlight;

import static org.openspaces.extensions.QueryExtension.count;

import java.util.HashMap;
import java.util.Map;

import main.Car;
import main.Direction;
import main.Position;
import main.Roxel;
import main.RoxelInfo;
import main.SimulationParameters;

import org.openspaces.core.GigaSpace;

import com.gigaspaces.client.WriteModifiers;
import com.j_spaces.core.client.SQLQuery;

public class MediumIntelligentTrafficLightSwitschingAlgorithm implements TrafficLightSwitchingAlgorithm {
	
	private GigaSpace space;
	private Map<Position, RoxelInfo>  worldMap              = new HashMap<>();
	private Map<Long, RoxelInfo>      roxelIdRoxelInfoMap   = new HashMap<>();
	//private Map<Long, DirectionLease> roxelIdDirectionLease = new HashMap<>();
	

	public MediumIntelligentTrafficLightSwitschingAlgorithm(GigaSpace space){
		this.space = space;
		for (RoxelInfo ri : space.readMultiple(new RoxelInfo())) {
			worldMap.put(ri.getPosition(), ri);
			roxelIdRoxelInfoMap.put(ri.getRoxelId(), ri);
		}
	}
	
	@Override
	public DirectionLease getNextDirection(Roxel rx) {
		
		RoxelInfo ri = roxelIdRoxelInfoMap.get(rx.getId());
	
		//count cars that want to drive in the given directions
		SQLQuery<Car> countCarsWantingEastQuery  = getWaitingCarsQuery(Direction.east, ri.getPosition());
		SQLQuery<Car> countCarsWantingSouthQuery = getWaitingCarsQuery(Direction.south, ri.getPosition());
		
		//count cars that are already standing o the other side of the crossing
		SQLQuery<Car> countCarsAlreadyStandingSouthQuery = getWaitingCarsQuery(Direction.south, getPositionOfNextCrossingInDirection(ri.getPosition(), Direction.south));
		SQLQuery<Car> countCarsAlreadyStandingEastQuery  = getWaitingCarsQuery(Direction.east, getPositionOfNextCrossingInDirection(ri.getPosition(), Direction.east));
		
		

		Long cntWntE = count(space, countCarsWantingEastQuery, "id");
		Long cntWntS = count(space, countCarsWantingSouthQuery, "id");
		
		Long cntAlrdE = count(space, countCarsAlreadyStandingEastQuery, "id");
		Long cntAlrdS = count(space, countCarsAlreadyStandingSouthQuery, "id");
		
		
		
		Long cntFreeRoxelsE = SimulationParameters.DISTANCE_BETW_STREETS_X - cntAlrdE -1;
		Long cntFreeRoxelsS = SimulationParameters.DISTANCE_BETW_STREETS_Y - cntAlrdS -1;
		
		 
		Long cntCanDriveE = cntWntE > cntFreeRoxelsE ? cntFreeRoxelsE : cntWntE;
		
		Long max = cntCanDriveE;
		Direction dirMax = Direction.east;
		
//			if(cntE > cntN){
//				max = cntE;
//				dirMax = Direction.east;
//			}
		Long cntCanDriveS = cntWntS > cntFreeRoxelsS ? cntFreeRoxelsS : cntWntS;
		if(cntCanDriveS > max){
			max = cntCanDriveS;
			dirMax = Direction.south;
		}
		
		System.out.println("cntCanDriveS= " + cntCanDriveS + "; cntCanDriveE= " + cntCanDriveE );
//			if(cntW > max){
//				max = cntW;
//				dirMax = Direction.west;
//			}
//			
		Direction[] allowedDirections = null;
		
		//max--;
		
		if(max <= 0){
			allowedDirections = new Direction[]{Direction.toDecide};
			//allowedDirections = new Direction[] {Direction.east, Direction.south};
			
			//roxelIdDirectionLease.put(rx.getId(), new DirectionLease(allowedDirections, 0L));
			System.out.println("########## no Cars can drive on "  + rx);
			return new DirectionLease(allowedDirections, 0L);
			
		}else{ //there are cars that are allowed to drive
			allowedDirections = new Direction[] {dirMax};
			//roxelIdDirectionLease.put(rx.getId(), new DirectionLease(allowedDirections, max -1));
			return new DirectionLease(allowedDirections, max);
		}
		
		
//		System.out.println("will wait for " + max + " cars on: " + roxelIdRoxelInfoMap.get(rx.getId()));
//		try {
//	        String query = "position.x=" + ri.getPosition().getX() + " AND position.y=" + ri.getPosition().getY(); 
//	        TrafficLightSignal tls = space.readIfExists(new SQLQuery<TrafficLightSignal>(TrafficLightSignal.class, query));
//	        if(tls == null){
//	            tls = new TrafficLightSignal();
//	        }
//	        tls.setPosition(ri.getPosition());
//	        tls.setDirection(new Direction[]{});
//	        space.write(tls, WriteModifiers.UPDATE_OR_WRITE);
//		    
//            Thread.sleep(SimulationParameters.TRAFFIC_LIGHT_SWITCHING_DELAY_MS);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
		//return allowedDirections;
		
	}
	
	/**
	 * 
	 * @param d {@code Direction } the cars are heading
	 * @param currentPos {@code Position} the waiting cars want to enter
	 * @return
	 */
	private SQLQuery<Car> getWaitingCarsQuery(Direction d, Position currentPos){
		
		String andOrOR     = "AND";
		String ordinate    = "y";	
		String notOrdinate = "x";
		
		
		if(d.equals(Direction.east) || d.equals(Direction.west)){
			ordinate = "x";
			notOrdinate = "y";
		}
		
		int maxPos        = ordinate.equals("x") ? currentPos.getX() : currentPos.getY();
		int minPos        = maxPos - (ordinate.equals("x") ? SimulationParameters.DISTANCE_BETW_STREETS_X : SimulationParameters.DISTANCE_BETW_STREETS_Y);
		int posOnNotOrdinate = !ordinate.equals("x") ? currentPos.getX() : currentPos.getY();
		
		if(minPos < 0 ){
			minPos += SimulationParameters.WORLD_ROXEL_CNT_X;
			andOrOR = "OR";
		}
		
		return new SQLQuery<>(Car.class,  "position." + ordinate + " < " + maxPos + " " + andOrOR + " position." + ordinate + " > " + minPos 
				                        + " AND "
				                        + "position." + notOrdinate + " = " + posOnNotOrdinate);
		
	}
	
	private Position getPositionOfNextCrossingInDirection(Position p, Direction d){
		
		if(Direction.east.equals(d)){
			return new Position((p.getX() + SimulationParameters.DISTANCE_BETW_STREETS_X) % SimulationParameters.WORLD_ROXEL_CNT_X, p.getY());
		}else if(Direction.west.equals(d)){
			int newXPos = (p.getX() - SimulationParameters.DISTANCE_BETW_STREETS_X);
			if(newXPos < 0){
				newXPos += SimulationParameters.DISTANCE_BETW_STREETS_X;
			}
			return new Position(newXPos, p.getY());
		}else if(Direction.north.equals(d)){
			int newYPos = (p.getY() - SimulationParameters.DISTANCE_BETW_STREETS_Y);
			if(newYPos < 0){
				newYPos += SimulationParameters.DISTANCE_BETW_STREETS_Y;
			}
			return new Position(p.getX(), newYPos);
		}else if(Direction.south.equals(d)){
			return new Position(p.getX(), (p.getY() + SimulationParameters.DISTANCE_BETW_STREETS_Y) % SimulationParameters.WORLD_ROXEL_CNT_Y);
		}else{
			throw new IllegalArgumentException("invalid Direction " + d);
		}
	}

}
