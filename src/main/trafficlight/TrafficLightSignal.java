package main.trafficlight;

import main.Direction;
import main.Position;

import com.gigaspaces.annotation.pojo.SpaceClass;
import com.gigaspaces.annotation.pojo.SpaceId;

@SpaceClass
public class TrafficLightSignal {
    private String id;
    private Position position;
    private Direction[] direction;
    private DirectionLease dirLease;
    
    
    public DirectionLease getDirLease() {
		return dirLease;
	}

	public void setDirLease(DirectionLease dirLease) {
		this.dirLease = dirLease;
	}

	public TrafficLightSignal() {
    }

    @SpaceId(autoGenerate = true)
    public String getId() {
        return id;
    }
    
    @SpaceId(autoGenerate = true)
    public void setId(String id) {
        this.id = id;
    }
    public Position getPosition() {
        return position;
    }
    public void setPosition(Position position) {
        this.position = position;
    }
    public Direction[] getDirection() {
        return direction;
    }
    public void setDirection(Direction[] direction) {
        this.direction = direction;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((direction == null) ? 0 : direction.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((position == null) ? 0 : position.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TrafficLightSignal other = (TrafficLightSignal) obj;
        if (direction == null) {
            if (other.direction != null)
                return false;
        } else if (!direction.equals(other.direction))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (position == null) {
            if (other.position != null)
                return false;
        } else if (!position.equals(other.position))
            return false;
        return true;
    }
    
    
    
}
