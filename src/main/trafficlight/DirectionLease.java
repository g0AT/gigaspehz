package main.trafficlight;

import java.io.Serializable;

import main.Direction;

public class DirectionLease implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Direction[] dirs;
	private Long lease;
	
	public DirectionLease(Direction[] dirs, Long lease){
		this.dirs = dirs;
		this.lease = lease;
	}

	public Direction[] getDirs() {
		return dirs;
	}

	public Long getLease() {
		return lease;
	}

	public void setLease(Long lease) {
		this.lease = lease;
	}
}