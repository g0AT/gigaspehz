package main.trafficlight;

import main.Direction;
import main.Roxel;

public interface TrafficLightSwitchingAlgorithm {
	public DirectionLease getNextDirection(Roxel rx);
}
