package main;

import com.gigaspaces.annotation.pojo.SpaceClass;
import com.gigaspaces.annotation.pojo.SpaceId;

@SpaceClass
public class SimulationState {

	public enum State{
		STARTUP,
		MAP_CREATED,
		PU_INITIALIZED
	}
	
	private Long id;
	private State state;
	private Long puCnt;
	
	public Long getPuCnt() {
		return puCnt;
	}

	public void setPuCnt(Long puCnt) {
		this.puCnt = puCnt;
	}

	public SimulationState(){
		id = new Long(333333333333333L);
	}
	
	@SpaceId
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public State getState() {
		return state;
	}
	public void setState(State state) {
		this.state = state;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimulationState other = (SimulationState) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (state != other.state)
			return false;
		return true;
	}
	
	
		
	
}
