package main;

import java.io.Serializable;

import com.gigaspaces.annotation.pojo.SpaceClass;

@SpaceClass
public class Car extends AbstractCar implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	public Car(){		
		
	}
	
	public Car(Position position, Double speedMs, Direction direction){
		super( position, speedMs, direction );
	}
}