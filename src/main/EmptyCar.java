package main;

import java.io.Serializable;

import com.gigaspaces.annotation.pojo.SpaceClass;

@SpaceClass
public class EmptyCar extends AbstractCar implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EmptyCar(){
		super.setId(-1L);
	}
	
}
