package main;

import com.gigaspaces.annotation.pojo.SpaceClass;
import com.gigaspaces.annotation.pojo.SpaceId;

@SpaceClass
public class RoxelInfo {
	
	private static Long IDCOUNTER = new Long(2000);
	
	private Long id;
	private Long roxelId;
	private Position position;
//	private Direction direction;
	private Boolean dirNorth;
	private Boolean dirEast;
	private Boolean dirSouth;
	private Boolean dirWest;
	
	private Long lengthM;
	
	public RoxelInfo() {
		
	}
	
	public RoxelInfo(Roxel roxel, Position position, Direction ... directions){
		this.id = IDCOUNTER++;
		this.roxelId = roxel.getId();
		this.position = position;
		//this.direction = direction;
		setDirections(directions);
		this.lengthM = SimulationParameters.ROXEL_LENGTH_IN_METER;
	}
	
	public boolean isCrossing(){
		int cnt = 0;
		
		if(dirNorth != null && dirNorth){
			cnt++;
		}
		
		if(dirEast != null && dirEast){
			cnt++;
		}
		
		if(dirSouth != null && dirSouth){
			cnt++;
		}
		
		if(dirWest != null && dirWest){
			cnt++;
		}
		
		return cnt > 1;
	}

	public Position getPosition() {
		return position;
	}
	
	
	@SpaceId( autoGenerate = false )
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setPosition(Position position) {
		this.position = position;
	}
	

//	public Direction getDirection() {
//		return direction;
//	}
//
//	public void setDirection(Direction direction) {
//		this.direction = direction;
//	}
	
	public Long getLengthM() {
		return lengthM;
	}

	public Boolean getDirNorth() {
		return dirNorth;
	}

	public void setDirNorth(Boolean dirNorth) {
		this.dirNorth = dirNorth;
	}

	public Boolean getDirEast() {
		return dirEast;
	}

	public void setDirEast(Boolean dirEast) {
		this.dirEast = dirEast;
	}

	public Boolean getDirSouth() {
		return dirSouth;
	}

	public void setDirSouth(Boolean dirSouth) {
		this.dirSouth = dirSouth;
	}

	public Boolean getDirWest() {
		return dirWest;
	}

	public void setDirWest(Boolean dirWest) {
		this.dirWest = dirWest;
	}

	public void setLengthM(Long lengthM) {
		this.lengthM = lengthM;
	}

	public Long getRoxelId() {
		return roxelId;
	}

	public void setRoxelId(Long roxelId) {
		this.roxelId = roxelId;
	}

	public void setDirections(Direction ... directions){
		dirNorth = null;
		dirEast  = null;
		dirSouth = null;
		dirWest  = null;
		
		for(Direction d: directions){
			if(d.equals(Direction.north)){
				dirNorth = true;
			}else if(d.equals(Direction.east)){
				dirEast = true;
			}else if(d.equals(Direction.south)){
				dirSouth = true;
			}else if(d.equals(Direction.west)){
				dirWest = true;
			}else if(d.equals(Direction.blocked)){
				dirNorth = false;
				dirEast  = false;
				dirSouth = false;
				dirWest  = false;
			}
		}
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dirEast == null) ? 0 : dirEast.hashCode());
		result = prime * result
				+ ((dirNorth == null) ? 0 : dirNorth.hashCode());
		result = prime * result
				+ ((dirSouth == null) ? 0 : dirSouth.hashCode());
		result = prime * result + ((dirWest == null) ? 0 : dirWest.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((lengthM == null) ? 0 : lengthM.hashCode());
		result = prime * result
				+ ((position == null) ? 0 : position.hashCode());
		result = prime * result + ((roxelId == null) ? 0 : roxelId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RoxelInfo other = (RoxelInfo) obj;
		if (dirEast == null) {
			if (other.dirEast != null)
				return false;
		} else if (!dirEast.equals(other.dirEast))
			return false;
		if (dirNorth == null) {
			if (other.dirNorth != null)
				return false;
		} else if (!dirNorth.equals(other.dirNorth))
			return false;
		if (dirSouth == null) {
			if (other.dirSouth != null)
				return false;
		} else if (!dirSouth.equals(other.dirSouth))
			return false;
		if (dirWest == null) {
			if (other.dirWest != null)
				return false;
		} else if (!dirWest.equals(other.dirWest))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (lengthM == null) {
			if (other.lengthM != null)
				return false;
		} else if (!lengthM.equals(other.lengthM))
			return false;
		if (position == null) {
			if (other.position != null)
				return false;
		} else if (!position.equals(other.position))
			return false;
		if (roxelId == null) {
			if (other.roxelId != null)
				return false;
		} else if (!roxelId.equals(other.roxelId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RoxelInfo [id=" + id + ", roxelId=" + roxelId + ", position="
				+ position + ", dirNorth=" + dirNorth + ", dirEast=" + dirEast
				+ ", dirSouth=" + dirSouth + ", dirWest=" + dirWest
				+ ", lengthM=" + lengthM + "]";
	}


	

	

	

	
	
	
	
	
 	
}
