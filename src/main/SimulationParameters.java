package main;

public class SimulationParameters {
	
	public static final String TUPEL_SPACE_URI_REMOTE = "jini://*/*/TrafficCoordinationSpace";
	public static final String TUPEL_SPACE_URI = "/./TrafficCoordinationSpace";

	public static final int CAR_COUNT = 60 ;
	public static final int CAR_SPEED_METER_PER_SECOND = 20;
	public static final double CAR_SPEED_METER_PER_SECOND_VARIATION = 0.05;
	public static final long ROXEL_LENGTH_IN_METER = 5;
	
	public static final long TRAFFIC_LIGHT_SWITCHING_DELAY_MS = 1000;
	
	
	public static final int ROXEL_PIXEL_WIDTH = 64;
	
	public static final int DISTANCE_BETW_STREETS_X = 5;
	public static final int DISTANCE_BETW_STREETS_Y = 5;
	
	public static final int WORLD_ROXEL_CNT_X = 24;
	public static final int WORLD_ROXEL_CNT_Y = 24;
	
	public static final int SIMULATION_FRAMERATE = 60;
	public static final int SIMULATION_FRAMESKIP = 1;
	
	public static final int SIMULATION_SCALE_DIVIDER = 2;
	
	
	public static final int AMOUNT_TREES = 200;
	//public static final int 
	
}
