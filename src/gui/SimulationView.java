package gui;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import jgame.JGColor;
import jgame.JGFont;
import jgame.platform.JGEngine;
import main.Car;
import main.Direction;
import main.Position;
import main.RoxelInfo;
import main.SimulationParameters;
import main.trafficlight.TrafficLightSignal;

import org.openspaces.core.GigaSpace;

public class SimulationView extends JGEngine {

    /**
	 * 
	 */
    private static final long      serialVersionUID   = 1L;

    private List<CarObject>        allVisibleCars     = new LinkedList<>();
    private TrafficLightSignal[] tlss;
    private Map<Direction, String> directionImgStrMap = new HashMap<Direction, String>();

    private GigaSpace              tupelSpace;

    private int                    roxelCntX;
    private int                    roxelCntY;

    // public static void main(String[] args) {
    //
    // String gsURL = "jini://*/*/TrafficCoordinationSpace"; // remote space
    // GigaSpace tupelSpace = new GigaSpaceConfigurer(new
    // UrlSpaceConfigurer(gsURL)).gigaSpace();
    //
    // new SimulationView(SimulationParameters.WORLD_ROXEL_CNT_X,
    // SimulationParameters.WORLD_ROXEL_CNT_Y, tupelSpace);
    // }

    public SimulationView(int roxelCntX, int roxelCntY, GigaSpace tupelSpace) {
        this.roxelCntX = roxelCntX;
        this.roxelCntY = roxelCntY;
        this.tupelSpace = tupelSpace;
        initEngine(roxelCntX * SimulationParameters.ROXEL_PIXEL_WIDTH / SimulationParameters.SIMULATION_SCALE_DIVIDER, roxelCntY
                * SimulationParameters.ROXEL_PIXEL_WIDTH / SimulationParameters.SIMULATION_SCALE_DIVIDER);

        directionImgStrMap.put(Direction.north, "|");
        directionImgStrMap.put(Direction.east, "=");
        directionImgStrMap.put(Direction.south, "|");
        directionImgStrMap.put(Direction.west, "=");
        directionImgStrMap.put(Direction.create(Direction.east, Direction.south), "#");

//        System.out.println(directionImgStrMap.get(Direction.create(Direction.south, Direction.east)));
//        System.out.println(directionImgStrMap.get(Direction.south));
        // directionImgStrMap.put(Direction.BLOCKED, ".");
        // directionImgStrMap.put(Direction.TODECIDE, "#");
    }

    public void initCanvas() {
        setCanvasSettings(roxelCntX, roxelCntY, SimulationParameters.ROXEL_PIXEL_WIDTH, SimulationParameters.ROXEL_PIXEL_WIDTH, null, null,
                null);
    }

    public void initGame() {
        // we increase the framerate a bit because it is more critical
        // for scrolling.
        setFrameRate(SimulationParameters.SIMULATION_FRAMERATE, SimulationParameters.SIMULATION_FRAMESKIP);
        // load a single sprite
        defineImage("car", // graphic name
                "c", 1, // tile name and tile cid (in case we use it as a tile)
                "./res/pic/auto64.png", // file
                "-" // graphical operation (may be one or two out of
                    // "x","y", "l","r","u")
        );

        defineImage("carE", // graphic name
                "ce", 1, // tile name and tile cid (in case we use it as a tile)
                "./res/pic/auto64.png", // file
                "u" // graphical operation (may be one or two out of
                    // "x","y", "l","r","u")
        );

        defineImage("carS", // graphic name
                "cs", 1, // tile name and tile cid (in case we use it as a tile)
                "./res/pic/auto64.png", // file
                "l" // graphical operation (may be one or two out of
                    // "x","y", "l","r","u")
        );

        defineImage("tree", // graphic name
                "tr", 2, // tile name and tile cid (in case we use it as a tile)
                "./res/pic/tree64.png", // file
                "-" // graphical operation (may be one or two out of
                    // "x","y", "l","r","u")
        );

        defineImage("street_h", // graphic name
                "=", 2, // tile name and tile cid (in case we use it as a tile)
                "./res/pic/street64_h.png", // file
                "-" // graphical operation (may be one or two out of
                    // "x","y", "l","r","u")
        );

        defineImage("street_v", // graphic name
                "|", 2, // tile name and tile cid (in case we use it as a tile)
                "./res/pic/street64_v.png", // file
                "-" // graphical operation (may be one or two out of
                    // "x","y", "l","r","u")
        );

        defineImage("street_hv", // graphic name
                "#", 2, // tile name and tile cid (in case we use it as a tile)
                "./res/pic/street64_hv.png", // file
                "-" // graphical operation (may be one or two out of
                    // "x","y", "l","r","u")
        );

        defineImage("street_hvTodecide", // graphic name
                "#?", 2, // tile name and tile cid (in case we use it as a tile)
                "./res/pic/street64_hv.png", // file
                "-" // graphical operation (may be one or two out of
                    // "x","y", "l","r","u")
        );

        // load the background image
        defineImage("empty", ".", 0, "./res/pic/grass.jpg", "-");
        setBGImage("empty");

        // hide the mouse cursor
        // setCursor(null);
        // Increase the playfield size to obtain a scrollable, wrappable area.
        // It is twice the width of the view, and two tiles higher. The two
        // tiles are used as space for objects to disappear into before
        // wrapping around. Without this space, objects would visibly jump
        // from top to bottom or vice versa.
        setPFSize(roxelCntX, roxelCntY);
        // Set the wrap-around mode to vertical.
        setPFWrap(false, // horizontal wrap
                false, // vertical wrap
                -10, -10 // shift the center of the view to make objects wrap at
                         // the right moment (sprite size / 2).
        );

        RoxelInfo[] wholeWorld = tupelSpace.readMultiple(new RoxelInfo());

        for (RoxelInfo ri : wholeWorld) {
            setTile(ri.getPosition().getX(), ri.getPosition().getY(), getDirectionImageStr(ri));
        }

        RoxelInfo template = new RoxelInfo();
        template.setDirections(Direction.blocked);
        List<RoxelInfo> plantingSpace = new LinkedList<RoxelInfo>(Arrays.asList(tupelSpace.readMultiple(template)));

        int i = 0;
        while (i < SimulationParameters.AMOUNT_TREES && plantingSpace.size() > 0) {
            int idx = (int) (Math.random() * plantingSpace.size());
            RoxelInfo ri = plantingSpace.remove(idx);
            setTile(ri.getPosition().getX(), ri.getPosition().getY(), "tr");
            i++;
        }

        // Roxel[] roxelMapOfInterest = tupelSpace.readMultiple(new Roxel());

        // for (Roxel r : roxelMapOfInterest) {
        // setTile(r.getPos().getX(), r.getPos().getY(),
        // directionImgStrMap.get(r.getDir()));
        // }

        // RoxelInfo[] roxelMapOfInterest = tupelSpace.readMultiple(new
        // RoxelInfo());
        //
        // for(RoxelInfo r: roxelMapOfInterest){
        // setTile(r.getPosition().getX(), r.getPosition().getY(),
        // directionImgStrMap.get(r.getDir()));
        // }

    }

    /** View offset. */
    int     xofs   = 0, yofs = 0;

    boolean remove = false;

    public void doFrame() {
        tlss = tupelSpace.readMultiple(new TrafficLightSignal());
        
        Car[] allCars = tupelSpace.readMultiple(new Car());
        outer: for (Car c : allCars) {

            for (CarObject co : allVisibleCars) {
                if (co.getID() == c.getId()) {
//                    co.setPos(c.getPosition().getX() * SimulationParameters.ROXEL_PIXEL_WIDTH, c.getPosition().getY()
//                            * SimulationParameters.ROXEL_PIXEL_WIDTH);
                    
                    co.setRoxelPos(c.getPosition());
                    double speed = c.getSpeedMeterPerSecond() /  SimulationParameters.ROXEL_LENGTH_IN_METER;          
                    if(c.getDirection().equals(Direction.south)){
                        co.setSpeed(0, speed);   
                    }else if(c.getDirection().equals(Direction.east)){
                        co.setSpeed(speed, 0);
                    }
                    continue outer;
                }
            }
            CarObject coNew = new CarObject(new Position(c.getPosition().getX() * SimulationParameters.ROXEL_PIXEL_WIDTH, c.getPosition()
                    .getY() * SimulationParameters.ROXEL_PIXEL_WIDTH), c.getDirection(),  c.getSpeedMeterPerSecond(), c.getId());
            allVisibleCars.add(coNew);

        }

        moveObjects(null, 0);
        // Update the desired view offset according to mouse position.
        // The X offset is proportional to the mouse position.
        // The mouse position is a number between 0 and viewWidth.
        // We scale it to a number between 0 and pfWidth (playfield width).
        xofs = getMouseX() * pfWidth() / viewWidth();
        // the Y offset changes proportional to the offset of the mouse
        // position from the center of the window. If the mouse is in the
        // center, we don't scroll, if it is close to the upper or lower
        // border of the window, it scrolls quickly in that direction.
        // yofs += (getMouseY() - viewHeight()/2) / 15;
        yofs = getMouseY() * pfHeight() / viewHeight();
        // Set the view offset. Note that if our offset is out of the
        // playfield bounds, the position is clipped so that it is inside.
        // (this is only relevant for non-wrappable axes; a wrappable
        // axis is never out of bounds!)
        setViewOffset(xofs, yofs, // the position within the playfield
                true // true means the given position is center of the view,
                     // false means it is topleft.
        );

    }

    public void paintFrame() {
        for (TrafficLightSignal tls : tlss) {
            int x = tls.getPosition().getX() * SimulationParameters.ROXEL_PIXEL_WIDTH;
            int y = tls.getPosition().getY() * SimulationParameters.ROXEL_PIXEL_WIDTH;

            JGColor colN = new JGColor(1, 0, 0, 0.1);
            JGColor colE = new JGColor(1, 0, 0, 0.1);
            JGColor colS = new JGColor(1, 0, 0, 0.1);
            JGColor colW = new JGColor(1, 0, 0, 0.1);
            
            for(Direction d: tls.getDirection()){
                if(d.equals(Direction.north)){
                    colN = new JGColor(0, 1, 0, 0.1);
                }else if(d.equals(Direction.east)){
                    colE = new JGColor(0, 1, 0, 0.1);
                }else if(d.equals(Direction.south)){
                    colS = new JGColor(0, 1, 0, 0.1);
                }else if(d.equals(Direction.west)){
                    colW = new JGColor(0, 1, 0, 0.1);
                }
                
            }
            
            drawOval(x, y, 16, 16, true, true, 0.1, colS);
            drawOval(x, y + SimulationParameters.ROXEL_PIXEL_WIDTH -16, 16, 16, true, true, 0.1, colE);
            drawOval(x + SimulationParameters.ROXEL_PIXEL_WIDTH -16, y, 16, 16, true, true, 0.1, colN);
            drawOval(x + SimulationParameters.ROXEL_PIXEL_WIDTH -16, y + SimulationParameters.ROXEL_PIXEL_WIDTH -16, 16, 16, true, true, 0.1, colW);
            
            
        }

        for (CarObject c : allVisibleCars) {
            setColor(JGColor.white);
            setFont(new JGFont("Arial", JGFont.BOLD, 25));
            drawString(c.getID().toString(), c.x + 28, c.y + 25, 0);
        }

    }

    private String getDirectionImageStr(RoxelInfo ri) {

        if (((ri.getDirSouth() != null && ri.getDirSouth()) || ((ri.getDirNorth() != null && ri.getDirNorth())))
                && ((ri.getDirEast() != null && ri.getDirEast()) || ((ri.getDirWest() != null && ri.getDirWest())))) {
            return "#";
        } else if ((ri.getDirEast() != null && ri.getDirEast()) || (ri.getDirWest() != null && ri.getDirWest())) {
            return "=";
        } else if ((ri.getDirNorth() != null && ri.getDirNorth()) || (ri.getDirSouth() != null && ri.getDirSouth())) {
            return "|";
        } else if (ri.getDirNorth() == null && ri.getDirEast() == null && ri.getDirSouth() == null && ri.getDirWest() == null) {
            return "#?";
        } else if (ri.getDirSouth() != null && !ri.getDirSouth() && ri.getDirNorth() != null && !ri.getDirNorth()
                && ri.getDirEast() != null && !ri.getDirEast() && ri.getDirWest() != null && !ri.getDirWest()) {
            return ".";
        } else {
            throw new IllegalArgumentException("no matching tile-string for this combination of directions: " + ri);
        }

    }

}
