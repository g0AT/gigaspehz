package gui;

import main.Direction;
import main.Position;
import main.SimulationParameters;
import jgame.JGObject;

public class CarObject extends JGObject {

    private final Long id;
    private double     speedMs;
    private Position   posTarget;
    private Direction  dir;

    public CarObject(Position position, Direction direction, double speedMs, Long id) {

        super("car", true, position.getX(), position.getY(), 0, direction.equals(Direction.east) ? "carE" : "carS");
        this.id = id;
        this.dir = direction;
        this.posTarget = new Position(position.getX() / SimulationParameters.ROXEL_PIXEL_WIDTH, position.getY()
                / SimulationParameters.ROXEL_PIXEL_WIDTH);
        this.speedMs = speedMs;
    }

    @Override
    public void move() {
        int xPosEnd = posTarget.getX() * SimulationParameters.ROXEL_PIXEL_WIDTH;
        int yPosEnd = posTarget.getY() * SimulationParameters.ROXEL_PIXEL_WIDTH;
        if (dir.equals(Direction.south)) {
            if (y >= yPosEnd) {
                yspeed = 0;
                if (posTarget.getY() == 0) {
                    y = -SimulationParameters.ROXEL_PIXEL_WIDTH;
                    yspeed = speedMs / SimulationParameters.ROXEL_LENGTH_IN_METER;
                }
            }
        } else if (dir.equals(Direction.east)) {
            if (x >= xPosEnd) {
                xspeed = 0;
                if (posTarget.getX() == 0) {
                    x = -SimulationParameters.ROXEL_PIXEL_WIDTH;
                    xspeed = speedMs / SimulationParameters.ROXEL_LENGTH_IN_METER;
                }
            }
        }

    };

    public Position getRoxelPos() {
        return posTarget;
    }

    public void setRoxelPos(Position pos) {
        this.posTarget = pos;
    }

    public Long getID() {
        return id;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CarObject other = (CarObject) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

}
